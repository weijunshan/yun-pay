<?php
// +----------------------------------------------------------------------
// | Created by PhpStorm.
// +----------------------------------------------------------------------
// | user : 刘强
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/7/31 10:51
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\Data\Pay;

use liuQiang\yunPay\Data\Pay\BaseData;
use liuQiang\yunPay\Data\Router;

class BankPayData extends BaseData
{
    /**
     * 银⾏开户卡号(必填)
     * @var
     */
    public $card_no;

    /**
     * ⽤户或联系⼈⼿机号(选填)
     * @var
     */
    public $phone_no;

    /**
     * 收款⼈id(选填)
     * @var
     */
    public $anchor_id;

    protected $route = Router::BANK_CARD;
}
