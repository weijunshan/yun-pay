<?php
// +----------------------------------------------------------------------
// | Created by PhpStorm.
// +----------------------------------------------------------------------
// | user : 刘强
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/7/26 15:13
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\Data\Pay;

use liuQiang\yunPay\Data\Pay\BaseData;
use liuQiang\yunPay\Data\Router;

class AliPayData extends BaseData
{
    /**
     * 收款⼈⽀付宝账户(必填)
     * @var
     */
    public $card_no;

    /**
     * 校验⽀付宝账户姓名，可填 Check、NoCheck
     * @var string
     */
    public $check_name = 'NoCheck';

    protected $route = Router::ALI_PAY;
}
