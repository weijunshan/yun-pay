<?php
// +----------------------------------------------------------------------
// | Created by PhpStorm.
// +----------------------------------------------------------------------
// | user : 刘强
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/7/31 10:48
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\Data\Pay;

use liuQiang\yunPay\Data\Pay\BaseData;
use liuQiang\yunPay\Data\Router;

class WxPayData extends BaseData
{
    /**
     * 商户AppID下，某⽤户的openid(必填)
     * @var
     */
    public $openid;

    /**
     * 微信打款商户微信AppID(选填，最⼤⻓度为200)
     * @var
     */
    public $wx_app_id;

    /**
     * 微信打款模式(选填，⼆种取值，可填 "", "transfer")
     * @var string
     */
    public $wxpay_mode = "";

    protected $route = Router::WX_PAY;
}
