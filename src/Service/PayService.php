<?php
// +----------------------------------------------------------------------
// | Created by PhpStorm.
// +----------------------------------------------------------------------
// | user : 刘强
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/7/26 15:24
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\Service;

use liuQiang\yunPay\AbstractInterfaceTrait\BaseService;
use liuQiang\yunPay\AbstractInterfaceTrait\DataTrait;

class PayService extends BaseService
{
    protected $dealer_broker = true;

    use DataTrait;
}
