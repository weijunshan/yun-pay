<?php
// +----------------------------------------------------------------------
// | Created by PhpStorm.
// +----------------------------------------------------------------------
// | user : 刘强
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/8/1 11:40
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\AbstractInterfaceTrait;


trait MethodTypeTrait
{
    /**
     * 操作类型
     * @var string
     */
    protected $methodType = null;

    public function setMethodType($type): self
    {
        if(!in_array($type, self::METHOD_ARR))
        {
            throw new \Exception('method type error');
        }
        $this->methodType = $type;
        return $this;
    }
}
