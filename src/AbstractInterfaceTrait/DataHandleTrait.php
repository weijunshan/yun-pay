<?php
// +----------------------------------------------------------------------
// | Created by LW刘强.
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/4/12 10:39 上午
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\AbstractInterfaceTrait;


use liuQiang\yunPay\Service\Des3Service;

trait DataHandleTrait
{
    protected function callback(array $res)
    {
        if (isset($res['data']) && is_string($res['data'])) {
            $res['data'] = Des3Service::decode($res['data'], $this->config->des3_key);
        }
        return $res;
    }
}