<?php
// +----------------------------------------------------------------------
// | Created by LW刘强.
// +----------------------------------------------------------------------
// | blog : www.shuzi88.com
// +----------------------------------------------------------------------
// | email: 872871448@qq.com
// +----------------------------------------------------------------------
// | Date : 2021/4/9 8:33 上午
// +----------------------------------------------------------------------


namespace liuQiang\yunPay\Exception;

class YunPayException extends \Exception
{
    /**
     * 2021/4/9 9:10 上午
     * @param string $message
     * @throws YunPayException
     */
    public static function throwSelf(string $message)
    {
        throw new YunPayException($message);
    }
}